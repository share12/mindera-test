export const orders = require('./orders.json');

export const getOrderById = id =>
  orders.find(o => o.orderId == id);

export const getTotalPrice = order => {
  console.log(order.items.reduce((sum, item) => sum + item.quantity * item.price, 0));
  return order.items.reduce((sum, item) => sum + item.quantity * item.price, 0);
}
