
export function memoize(fn) {

  return new Proxy(fn, {
    cache: new Map(),
    apply(target, prop, args){
     
      
      let key = JSON.stringify(args)
     
      if(!this.cache.has(key)){
        this.cache.set(key, target.apply(prop, args))
      }
      // console.log(this.cache);
      return this.cache.get(key)
    }
  })

  // let cache = {};
  // return (...params) => {
  //   // const args = [].slice.call(arguments)
  //   let n = params[0]
    
  //   if (n in cache) {
  //     console.log('This is something in cache object');
  //     return cache[n];
  //   }else {
  //     console.log('This is nothing in the cache object');
  //     let result = fn(n);
  //     cache[n] = result;
  //     return result;
  //   }
  // }
}

export function cache(fn) {

  const cached = {}

  cached[fn] = memoize(fn)
 
  return {
    fn: cached[fn],
  }
  
}